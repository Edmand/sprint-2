USE [GlobalSTORE]
GO

/****** Object:  Table [dbo].[RGS_SmilesLeader]    Script Date: 9/10/2014 2:57:15 PM ******/
DROP TABLE [dbo].[RGS_SmilesLeader]
GO

/****** Object:  Table [dbo].[RGS_SmilesLeader]    Script Date: 9/10/2014 2:57:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RGS_SmilesLeader](
	[StoreNo] [smallint] NOT NULL,
	[EmplID] [nvarchar](12) NOT NULL,
	[StartDttm] [datetime] NOT NULL,
	[UntilDttm] [datetime] NOT NULL DEFAULT ('1900-01-01'),
	[IsCoached] [nchar](1) NOT NULL DEFAULT ('N'),
	[CoachedBy] [nvarchar](20) NOT NULL DEFAULT (' '),
	[CoachedByEmplId] [nvarchar](12) NOT NULL DEFAULT (''),
	[SmilesLeaderEventId] [int] NOT NULL,
	[BegEventCd] [nchar](1) NOT NULL DEFAULT (' '),
	[EndEventCd] [nchar](1) NOT NULL DEFAULT (' '),
 CONSTRAINT [PK_RGS_SmilesLeader_1] PRIMARY KEY CLUSTERED 
(
	[StoreNo] ASC,
	[StartDttm] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO


