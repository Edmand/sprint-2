USE [GlobalSTORE]
GO

DECLARE  @currentDate datetime
		,@noDate datetime

SET @noDate = '1900-01-01 00:00:00'
SET @currentDate = '2014-10-01 00:00:00'


SELECT Emp.FirstName, Emp.Status, Emp.SecGrpID, Emp.FullOrPartTime, Emp.StartDate, SL.* 
FROM RGS_SmilesLeaderEvent SL
	LEFT JOIN RGS_Employee Emp ON Emp.EmplID = SL.EmplID
WHERE StartDttm >= @currentDate --AND UntilDttm = @noDate
ORDER BY SmilesLeaderEventID DESC

SELECT Emp.FirstName, Emp.Status, Emp.SecGrpID, Emp.FullOrPartTime, Emp.StartDate, TS.* 
FROM RGS_TimeSheetDtl TS
	LEFT JOIN RGS_Employee Emp ON Emp.RegID = TS.EmpID
WHERE DateTimeIn >= @currentDate AND DateTimeOut = @noDate

SELECT * FROM RGS_System_Values WHERE SystId = 'SmilesLeaderName'

--delete from rgs_smilesleader where firstname is null
