USE [GlobalSTORE]
GO
/****** Object:  StoredProcedure [dbo].[usp_CloseAllOpenSmilesLeaderRecord]    Script Date: 9/2/2014 3:42:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_CloseAllOpenSmilesLeaderRecord]
	-- Add the parameters for the stored procedure here
	@timeSlice datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @noDate datetime
	
	SET @noDate = '1900-01-01 00:00:00'

	-- Close all open smiles leader records for previous time slice.
	UPDATE SmilesLeader SET
		UntilDttm = @timeSlice
	WHERE UntilDttm = @noDate
END
