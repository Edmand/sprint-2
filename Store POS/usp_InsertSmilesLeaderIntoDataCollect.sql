USE [GlobalSTORE]
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertSmilesLeaderIntoDataCollect]    Script Date: 9/12/2014 4:09:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edmand Looi
-- Create date: 9/9/2014
-- Description:	Insert Smiles Leader record into data collect table.
-- =============================================
ALTER PROCEDURE [dbo].[usp_InsertSmilesLeaderIntoDataCollect] 
	-- Add the parameters for the stored procedure here
	@SmilesLeaderEventID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	-- Insert new record and dynamically build XML to RGS_DataCollect table.
	INSERT INTO RGS_DataCollect (
		 Dc_Code
		,Dc_Version
		,Store_No
		,Dc_XML_Obj
		,Transmit_DtTm
		,DateStamp)
	SELECT
		'SMILES'
		,'1'
		,StoreNum
		,'<?xml version="1.0"?><DS_SMILESLEADER><SMILESLEADER><StoreNumber>'+CONVERT(varchar(12), StoreNum)+'</StoreNumber><EmployeeID>'+EmplID+'</EmployeeID><StartTimeStamp>'+convert(varchar(25), StartDttm, 121)+'</StartTimeStamp><EndTimeStamp>'+convert(varchar(25), UntilDttm, 121)+'</EndTimeStamp><SmilesLeaderEventID>'+CONVERT(varchar(12), SmilesLeaderEventID)+'</SmilesLeaderEventID><EventType>'+EventType+'</EventType></SMILESLEADER></DS_SMILESLEADER>'
		,{ts'1900-01-01 00:00:00'}
		,GETDATE()
	FROM RGS_SmilesLeaderEvent
	WHERE SmilesLeaderEventID = @SmilesLeaderEventID	

	RETURN

END
