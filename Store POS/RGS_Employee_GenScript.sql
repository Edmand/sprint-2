USE [GlobalSTORE]
GO

/****** Object:  Table [dbo].[RGS_Employee]    Script Date: 9/10/2014 3:22:42 PM ******/
DROP TABLE [dbo].[RGS_Employee]
GO

/****** Object:  Table [dbo].[RGS_Employee]    Script Date: 9/10/2014 3:22:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RGS_Employee](
	[EmplID] [nvarchar](12) NOT NULL DEFAULT (0),
	[Status] [char](1) NOT NULL DEFAULT (' '),
	[LastName] [nvarchar](50) NOT NULL DEFAULT (''),
	[FirstName] [nvarchar](50) NOT NULL DEFAULT (''),
	[Middlename] [nvarchar](50) NOT NULL DEFAULT (''),
	[PhoneNumber] [nvarchar](24) NOT NULL DEFAULT (''),
	[EmailAddr] [nvarchar](70) NOT NULL DEFAULT (''),
	[FullorPartTime] [char](1) NOT NULL DEFAULT (' '),
	[StartDate] [datetime] NOT NULL DEFAULT (getdate()),
	[EndDate] [datetime] NOT NULL DEFAULT ('2999-12-13'),
	[RegId] [nvarchar](12) NOT NULL DEFAULT (''),
	[SecGrpId] [smallint] NOT NULL DEFAULT (0),
	[RgsMenuCd] [char](1) NOT NULL DEFAULT (' '),
	[UpdatedBy] [nvarchar](12) NOT NULL DEFAULT (''),
	[DtStamp] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_RGS_Employee] PRIMARY KEY CLUSTERED 
(
	[EmplID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


