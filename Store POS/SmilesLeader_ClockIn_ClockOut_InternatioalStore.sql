USE [GlobalSTORE]
GO

DECLARE @noDate datetime
		,@weekBegin datetime
		,@SmilesLeaderName nvarchar(100)

SET @noDate = '1900-01-01 00:00:00'
SET @weekBegin = '2014-08-31 00:00:00'
SET @SmilesLeaderName = ''

--=-=-=-=-=-=-=-=-=-=-Command Goes Here-=-=-=-=-=-=-=-=-=-=-=-=-=-
-- Clock-Out Alice (A,30,F)
UPDATE RGS_TimeSheetDtl SET
	DateTimeOut = GETDATE()
WHERE EmpID = '1999' AND DateTimeOUt = @noDate

--=-=-=-=-=-=-=-=-=-=-Command Goes Here-=-=-=-=-=-=-=-=-=-=-=-=-=-


-- Call the Assign Smiles Leader store procudure
EXEC usp_AssignSmilesLeader @SmilesLeaderName OUTPUT
PRINT N'Return Smiles Leader is ' + @SmilesLeaderName

/*
-=-=-=-=-=-=-=-=-=-=-CLOCK OUT=-=-=-=-=-=-=-=-=-=-=-=-=-

-- Clock-In Alice (A,30,F)
INSERT INTO RGS_TimeSheetDtl (
	 WeekBegin
    ,EmpId
	,TypeOfHours
	,OrigDateTimeIn
	,DateTimeIn
) VALUES (
	 '2014-08-31 00:00:00'
	,'1999'
	,0
	,GETDATE()
	,GETDATE()
)

-- Clock-In Zachary Zekker (A,25,P)
INSERT INTO RGS_TimeSheetDtl (
	 WeekBegin
    ,EmpId
	,TypeOfHours
	,OrigDateTimeIn
	,DateTimeIn
) VALUES (
	 '2014-08-31 00:00:00'
	,'6165'
	,0
	,GETDATE()
	,GETDATE()
)

-- Clock-In Rose (S,20,Blank)
INSERT INTO RGS_TimeSheetDtl (
	 WeekBegin
    ,EmpId
	,TypeOfHours
	,OrigDateTimeIn
	,DateTimeIn
) VALUES (
	 '2014-08-31 00:00:00'
	,'6141'
	,0
	,GETDATE()
	,GETDATE()
)


-- Clock-In Kathy (A,20,F)
INSERT INTO RGS_TimeSheetDtl (
	 WeekBegin
    ,EmpId
	,TypeOfHours
	,OrigDateTimeIn
	,DateTimeIn
) VALUES (
	 '2014-08-31 00:00:00'
	,'7142'
	,0
	,GETDATE()
	,GETDATE()
)

-- Clock-In Patty (S,25,Blank)
INSERT INTO RGS_TimeSheetDtl (
	 WeekBegin
    ,EmpId
	,TypeOfHours
	,OrigDateTimeIn
	,DateTimeIn
) VALUES (
	 '2014-08-31 00:00:00'
	,'6124'
	,0
	,GETDATE()
	,GETDATE()
)


-=-=-=-=-=-=-=-=-=-=-CLOCK OUT=-=-=-=-=-=-=-=-=-=-=-=-=-

-- Clock-Out Alice (A,30,F)
UPDATE RGS_TimeSheetDtl SET
	DateTimeOut = GETDATE()
WHERE EmpID = '1999' AND DateTimeOUt = @noDate

-- Clock-Out Zachary (A,25,P)
UPDATE RGS_TimeSheetDtl SET
	DateTimeOut = GETDATE()
WHERE EmpID = '6165' AND DateTimeOUt = @noDate

-- Clock-Out Rose (S,25,Blank)
UPDATE RGS_TimeSheetDtl SET
	DateTimeOut = GETDATE()
WHERE EmpID = '6141' AND DateTimeOUt = @noDate

-- Clock-Out Kathy (A,20,F)
UPDATE RGS_TimeSheetDtl SET
	DateTimeOut = GETDATE()
WHERE EmpID = '7142' AND DateTimeOUt = @noDate

-- Clock-Out Patty (S,25,Blank)
UPDATE RGS_TimeSheetDtl SET
	DateTimeOut = GETDATE()
WHERE EmpID = '6124' AND DateTimeOUt = @noDate

*/



