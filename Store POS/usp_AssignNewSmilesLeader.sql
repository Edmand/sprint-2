USE [GlobalSTORE]
GO
/****** Object:  StoredProcedure [dbo].[usp_AssignNewSmilesLeader]    Script Date: 9/10/2014 4:08:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Edmand Looi
-- Create date: 8/13/2014
-- Description:	This store procedure is used to determine/update Smiles Leader tables. Also it generate Smiles leader data collect record based on events. 
-- =============================================
ALTER PROCEDURE [dbo].[usp_AssignNewSmilesLeader]
	-- Add the parameters for the stored procedure here
	@NewSmilesLeaderEmployeeId varchar(12)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE  @no bit
			,@yes bit
			,@zero bit
			,@currentDatetime datetime
			,@currentMinute int
			,@noDate datetime
			,@timeSliceStart datetime
			,@storeNumber int
			,@isSmilesLeaderRecordExistInTimeSlice bit
			,@nextEventId int
			,@currentEventID int

		SET  @no = 0
		SET  @yes = 1
		SET  @zero = 0
		SET  @currentDatetime = DATEADD(minute, DATEDIFF(minute, '20080101', GetDate()), '20080101')
		SET  @currentMinute = @zero
		SET  @noDate = '2999-12-31 00:00:00.000'
		SET  @timeSliceStart = '1900-01-01 00:00:00.000'
		SET  @storeNumber = @zero
		SET  @isSmilesLeaderRecordExistInTimeSlice = @no
		SET  @nextEventId = @zero
		SET  @currentEventID = @zero

	-- Calculate beginning and ending of the 15 minutes time slice.
	SET @currentMinute = DATEPART(MINUTE,@currentDatetime)
	SELECT @timeSliceStart = DATEADD(MINUTE,-@currentMinute + (ROUND(@currentMinute/CAST(15 As float),0)*15) , @currentDatetime )

	-- Check if time slice already exist and with blank end date. 
	SELECT  @isSmilesLeaderRecordExistInTimeSlice = @yes
	FROM RGS_SmilesLeaderEvent
	WHERE StartDttm = @timeSliceStart
			AND UntilDttm = @noDate

	-- Get the current Smiles Leader Event Id.
	SELECT @currentEventID = MAX(SmilesLeaderEventId)
	FROM RGS_SmilesLeaderEvent

	-- Set the next Smiles Leader Event Id.
--	SET @nextEventId = ISNULL(@currentEventID, 0) + 1

	IF @isSmilesLeaderRecordExistInTimeSlice = @yes
		BEGIN
			-- Update existing open smiles leader record for SAME time slice with a new smiles leader employee ID.
			UPDATE RGS_SmilesLeaderEvent SET
				 EmplId = @NewSmilesLeaderEmployeeId
				,EventType = 'A'
				,UpdatedBy = 'usp_AssignNewSmilesLeader'
				,DtStamp = GETDATE()
			WHERE StartDttm = @timeSliceStart
					AND UntilDttm = @noDate

			-- Generate a data collect record for the update of Smiles Leader for the same time slice.
			EXEC usp_InsertSmilesLeaderIntoDataCollect @currentEventID

		END
	ELSE
		BEGIN
			-- Close all open smiles leader records for previous time slice.
			UPDATE RGS_SmilesLeaderEvent SET
				 UntilDttm = @timeSliceStart
				,UpdatedBy = 'usp_AssignNewSmilesLeader'
				,DtStamp = GETDATE()
			WHERE UntilDttm = @noDate

			-- Generate a data collect record for Smiles leader we just closed.
			EXEC usp_InsertSmilesLeaderIntoDataCollect @currentEventID	

			-- Get the store number for the Store
			SELECT @storeNumber = Str_Id 
			FROM Store_Status
		
			-- Insert new smiles leader record for the next highest role level.
			INSERT INTO RGS_SmilesLeaderEvent (
				 StoreNum
				,EmplId
				,StartDttm
				,UntilDttm
--				,SmilesLeaderEventId
				,EventType
				,UpdatedBy
				,DtStamp
			) Values (
				 @storeNumber
				,@newSmilesLeaderEmployeeId
				,@timeSliceStart
				,@noDate
--				,@nextEventId
				,'A'
				,'usp_AssignNewSmilesLeader'
				,GETDATE()
			) 

			-- Get the auto generate SmilesLeaderEventID after the insert.
			SET @nextEventId = SCOPE_IDENTITY();

			-- Generate a data collect record for the newly created Smiles Leader record.
			EXEC usp_InsertSmilesLeaderIntoDataCollect @nextEventId
		END
	RETURN
END
