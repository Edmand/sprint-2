/****** Script for SelectTopNRows command from SSMS  ******/

DECLARE @TimeSliceCount int = 0
DECLARE @DateTimeInterval datetime

-- Loop for 96 time slice. Every 15mins for the day for the specific date and the next day. (i.e. 96 slice of 15mins * 2)
WHILE @TimeSliceCount < 192
BEGIN

	-- Set datetime slice in 15mins interval.
	SET @DateTimeInterval = DATEADD(MINUTE, @TimeSliceCount * 15, '2014-08-31 00:00:00.000')

	-- Insert for all store for a specif time slice.
	INSERT INTO StoreMetricSummary (
		   [StoreNumber]
		  ,[StartTimeStamp]
		  ,[District]
		  ,[TrafficCount]
		  ,[SalesLeaderEmployeeID]
		  ,[SalesLeaderCoachedBy]
		  ,[SalesTransCount]
		  ,[SalesTransAmount]
		  ,[SalesTransAmountLocal]
		  ,[SalesTransUnitCount]
		  ,[ReturnedTransCount]
		  ,[ReturnedTransAmount]
		  ,[ReturnedTransAmountLocal]
		  ,[ReturnedTransUnitCount]
		  ,[ExchTransCount]
		  ,[ExchTransAmount]
		  ,[ExchTransAmountLocal]
		  ,[ExchTransUnitCount]
		  ,[CRMCount]
		  ,[CRMEmailCount]
		  ,[CRMNewLoyaltyCount]
		  ,[CRMAddressCount]
		  ,[CRMPhoneCount]
		  ,[CRMNameCount]
		  ,[CRMEligibleLoyaltyCount]
		  ,[SmilesLeaderEventId] )
	SELECT StoreNo
		  ,@DateTimeInterval
		  ,''
		  ,0
		  ,''
		  ,''
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
		  ,0
	FROM StoreHierarchy
--	WHERE EndDate = '2999-12-31' AND StoreNo = '9905'
	

	-- Increment the interval count.
	SET @TimeSliceCount += 1

END

