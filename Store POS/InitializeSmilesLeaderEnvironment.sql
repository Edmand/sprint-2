USE [GlobalSTORE]
GO

-- Initialize environment --
DECLARE  @noDate datetime 
		,@timeOutNoDate datetime
		,@currentDate datetime

SET @noDate = '2999-12-31 00:00:00'
SET @timeOutNoDate = '1900-01-01 00:00:00'
SET @currentDate = '2014-09-24 00:00:00'


-- Delete all Smiles leader record for the specific date.
UPDATE RGS_SmilesLeader SET
		 UntilDttm = StartDttm
WHERE UntilDttm = @timeOutNoDate

DELETE FROM RGS_SmilesLeader 
	WHERE StartDttm >= @currentDate

-- Reset the RGS_System_Values table.
UPDATE RGS_System_Values SET
		 SystValue = ''
		,DtStamp = @timeOutNoDate
WHERE SystId = 'SmilesLeaderName'

-- Close all open time sheet.
UPDATE RGS_TimesheetDtl SET
	DateTimeOut	= DateTimeIn
WHERE DateTimeOut = @timeOutNoDate

-- Remove all timesheet record for the specific date.
DELETE FROM RGS_TimeSheetDtl WHERE DateTimeIn >= @currentDate
	

