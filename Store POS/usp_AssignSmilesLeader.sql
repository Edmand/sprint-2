USE [GlobalSTORE]
GO
/****** Object:  StoredProcedure [dbo].[usp_AssignStoreLeader]    Script Date: 8/27/2014 4:12:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_AssignSmilesLeader]
	@SmilesLeaderName nvarchar(100) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Declare local variables
	DECLARE  @Yes bit 
			,@no bit
			,@isSmilesLeaderAssigned bit 
			,@isSmilesLeaderLoggedIn bit
			,@smilesLeaderEmployeeId nvarchar(12)
			,@smileLeaderBeginEventType nvarchar(1)
			,@noDate datetime
			,@timeOutNoDate datetime
			,@currentDate datetime
			,@autoAssign nvarchar(1)
			,@associate nvarchar(1)
			,@sharedResource nvarchar(1)
			,@loggedInEmployeeWithHighestRoleEmployeeId nvarchar(12)
			,@loggedInEmployeeWithHighestRoleFirstName nvarchar(50)
			,@loggedInEmployeeWithHighestRoleLastName nvarchar(50)
			,@loggedInEmployeeWithHighestRoleStatus nvarchar(1)
			,@loggedInEmployeeWithHighestRoleSecurityLevel smallint
			,@loggedInEmployeeWithHighestRoleFullOrPartTime nvarchar(1)
			,@loggedInEmployeeWithHighestRoleStartDate datetime
			,@shouldSetNewSmilesLeader bit

	-- Declare temporary table variable
	DECLARE  @EmployeeWithHighestRole table (
			 EmplId nvarchar(12) 
			,FirstName nvarchar(50)
			,LastName nvarchar(50)
			,CalculatedStatus nvarchar(1) 
			,Status nvarchar(1) 
			,SecGrpId smallint
			,FullOrPartTime nvarchar(1)
			,StartDate datetime
	)

	-- Initialize local variables
		SET  @yes = 1 
		SET	 @no = 0
		SET  @isSmilesLeaderAssigned = @no
	    SET  @isSmilesLeaderLoggedIn = @no
		SET  @smilesLeaderEmployeeId = ''
		SET  @smileLeaderBeginEventType =''
		SET  @noDate = '2999-12-31 00:00:00.000'
		SET  @timeOutNoDate = '1900-01-01 00:00:00.000'
		SET  @CurrentDate = DATEADD(minute, DATEDIFF(minute, '20080101', GetDate()), '20080101')
		SET  @autoAssign = 'A'
		SET  @associate = 'A'
		SET	 @sharedResource = 'S'
		SET  @loggedInEmployeeWithHighestRoleEmployeeId = ''
		SET  @loggedInEmployeeWithHighestRoleFirstName = ''
		SET  @loggedInEmployeeWithHighestRoleLastName = ''
		SET  @loggedInEmployeeWithHighestRoleStatus = ''
		SET  @loggedInEmployeeWithHighestRoleSecurityLevel = 0
		SET  @loggedInEmployeeWithHighestRoleFullOrPartTime = ''
		SET  @loggedInEmployeeWithHighestRoleStartDate = ''
		SET  @shouldSetNewSmilesLeader = @no
		SET  @SmilesLeaderName = ''

	-- Get current smiles leader record.
	SELECT  @isSmilesLeaderAssigned = @yes
		   ,@smilesLeaderEmployeeId = SL.EmplId
		   ,@smileLeaderBeginEventType = SL.EventType
	FROM RGS_SmilesLeaderEvent SL
	WHERE UntilDttm = @noDate

	-- Check if current smiles leader that was manually assigned is still logged in, if not, reassign a new smiles leader.
	SELECT  @isSmilesLeaderLoggedIn = @yes
	FROM RGS_Employee Emp
	INNER JOIN RGS_TimeSheetDtl TS ON Emp.RegId = TS.EmpId
	WHERE Emp.EmplId = @smilesLeaderEmployeeId AND TS.DateTimeOut = @timeOutNoDate

	-- Get logged in employee with the highest security level record.
	INSERT INTO @EmployeeWithHighestRole (
			 EmplId
			,FirstName
			,LastName
			,CalculatedStatus
			,Status
			,SecGrpId
			,FullOrPartTime
			,StartDate)
	SELECT  TOP 1 
		    MAX(Emp.EmplId) AS EmplId
		   ,MAX(FirstName) AS FirstName
		   ,MAX(LastName) AS LastName
		   ,MAX(CASE
					WHEN Emp.SecGrpId BETWEEN 28 AND 35 THEN 'A'
					ELSE Emp.Status
				END) AS CalculatedStatus
		   ,MAX(Emp.Status) AS Status
		   ,MAX(Emp.SecGrpId) AS SecGrpId
		   ,MAX(Emp.FullOrPartTime) As FullOrPartTime
		   ,MAX(Emp.StartDate) As StartDate
	FROM RGS_Employee Emp
	INNER JOIN RGS_TimeSheetDtl TS ON Emp.RegId = TS.EmpId AND Emp.Status IN (@associate, @sharedResource)
	WHERE TS.DateTimeOut = @timeOutNoDate
	GROUP BY EmpID
	ORDER BY CalculatedStatus, Emp.SecGrpId DESC, Emp.Status, Emp.FullOrPartTime, Emp.StartDate

	-- Get the employee with the highest role record into the variables.
	SELECT  @loggedInEmployeeWithHighestRoleEmployeeId = EmplId
		   ,@loggedInEmployeeWithHighestRoleFirstName = FirstName
		   ,@loggedInEmployeeWithHighestRoleLastName = LastName
		   ,@loggedInEmployeeWithHighestRoleStatus = Status
		   ,@loggedInEmployeeWithHighestRoleSecurityLevel = SecGrpId
		   ,@loggedInEmployeeWithHighestRoleFullOrPartTime = FullOrPartTime
		   ,@loggedInEmployeeWithHighestRoleStartDate = StartDate
	FROM @EmployeeWithHighestRole


	-- Check if someone is currently assign as Smiles Leader. If no, then assigned a new smile leader.
	IF @isSmilesLeaderAssigned = @no
	BEGIN
		PRINT N'Smiles Leader is not assigned'
		SET @shouldSetNewSmilesLeader = @yes
	END
	ELSE
	BEGIN
		-- When smiles leader is previously auto assigned and smiles leader does not have the highest role.
		IF @smileLeaderBeginEventType = @autoAssign AND @smilesLeaderEmployeeId != @loggedInEmployeeWithHighestRoleEmployeeId
		BEGIN
			PRINT N'Auto assigned but current Smiles Leader does not have the highest role.'
			SET @shouldSetNewSmilesLeader = @yes
		END
		ELSE 
		BEGIN
			-- If current smiles leader is not logged in. Assign new smiles leader.
			IF @isSmilesLeaderLoggedIn = @no
			BEGIN
				PRINT N'Current Smiles Leader is not logged in.'
				SET @shouldSetNewSmilesLeader = @yes
			END
		END
	END

	IF @shouldSetNewSmilesLeader = @yes
	BEGIN
		PRINT N'Calling Assign New Smiles Leader with LoggedInEmployeeWithHighestRoleEmployeeId = ' + @loggedInEmployeeWithHighestRoleEmployeeId
		-- Insert a new record for the loggedIn associate with the highest security level
		EXEC usp_AssignNewSmilesLeader @loggedInEmployeeWithHighestRoleEmployeeId

		SET @SmilesLeaderName = @loggedInEmployeeWithHighestRoleFirstName + ' ' + @loggedInEmployeeWithHighestRoleLastName	

		PRINT N'Updating RGS_System_Values with LoggedInEmployeeWithHighestRoleEmployeeId = ' + @loggedInEmployeeWithHighestRoleEmployeeId
		-- Update RGS_System_Values table with new Smiles Leader.
		UPDATE RGS_System_Values SET
			 SystValue = @SmilesLeaderName
			,DtStamp = GETDATE()
		WHERE SystId = 'SmilesLeaderName'
	END


	RETURN
END
